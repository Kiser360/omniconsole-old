import type { Route } from "./Router/router.service";
import Dashboard from './Views/Dashboard.svelte';
import GBCPlay from './Views/GBCPlay.svelte';

const Routes: Route[] = [
    { path: '/', component: Dashboard },
    { path: '/gbc/:id/play', component: GBCPlay },

];

export default Routes;