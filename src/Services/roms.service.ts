import { BehaviorSubject, Observable } from 'rxjs';

const allRoms$ = new BehaviorSubject([]);
const romsMap = new Map<RomEntry['id'], RomEntry>();
const binsMap = new Map<RomEntry['binId'], Blob>();

const RomsService = {
    getAll(platform: string): Observable<RomEntry[]> {
        return allRoms$;
    },
    async add(name: string, platform: string, bin: Blob) {
        const id = Math.random().toString(36).substring(2);
        const binId = Math.random().toString(36).substring(2);
        const saveBinId = Math.random().toString(36).substring(2);
        romsMap.set(id, {
            id,
            name,
            platform,
            createdAt: Date.now(),
            size: bin.size,
            binId,
            saveBinId,
        });
        binsMap.set(binId, bin);
        allRoms$.next(Array.from(romsMap.values()));
    },
    async getBin(id: RomEntry['id']): Promise<Blob> {
        return binsMap.get(id) || null;
    }
}

export default RomsService;

export type RomEntry = {
    id: string;
    name: string;
    platform: string;
    createdAt: number;
    size: number;
    binId: string;
    saveBinId: string;
}
