import { AUDIO_FRAMES, BinjgbAudio } from "./audio";
import BinjgbStore from "./Binjgb.store";
import { BinjgbGamepad } from "./gamepad";
import { makeWasmBuffer } from "./makeWasmBuffer";
import { BinjgbRewind } from "./rewind";
import { BinjgbVideo } from "./video";

const REWIND_FACTOR = 1.5;
const REWIND_UPDATE_MS = 16;
const CPU_TICKS_PER_SECOND = 4194304;
const EVENT_NEW_FRAME = 1;
const EVENT_AUDIO_BUFFER_FULL = 2;
const EVENT_UNTIL_TICKS = 4;
const MAX_UPDATE_SEC = 5 / 60;

export class BinjgbEmulator {
    static instance: BinjgbEmulator;

    static start(module, romBuffer, extRamBuffer, canvasEl: HTMLCanvasElement) {
        BinjgbEmulator.stop();
        BinjgbEmulator.instance = new BinjgbEmulator(module, romBuffer, extRamBuffer, canvasEl);
        BinjgbEmulator.instance.run();
        return BinjgbEmulator.instance;
    }

    static stop() {
        if (BinjgbEmulator.instance) {
            BinjgbEmulator.instance.destroy();
            BinjgbEmulator.instance = null;
        }
    }

    module;
    romDataPtr;
    e;
    gamepad;
    audio;
    video;
    rewind;
    rewindIntervalId;
    lastRafSec;
    leftoverTicks;
    fps;
    rafCancelToken;

    constructor(module, romBuffer, extRamBuffer, canvasEl) {
        this.module = module;
        this.romDataPtr = this.module._malloc(romBuffer.byteLength);
        makeWasmBuffer(this.module, this.romDataPtr, romBuffer.byteLength)
            .set(new Uint8Array(romBuffer));
        this.e = this.module._emulator_new_simple(
            this.romDataPtr, romBuffer.byteLength, BinjgbAudio.ctx.sampleRate,
            AUDIO_FRAMES);
        if (this.e == 0) {
            throw new Error('Invalid ROM.');
        }

        this.gamepad = new BinjgbGamepad(module, this.e);
        this.audio = new BinjgbAudio(module, this.e);
        this.video = new BinjgbVideo(module, this.e, canvasEl);
        this.rewind = new BinjgbRewind(module, this.e);
        this.rewindIntervalId = 0;

        this.lastRafSec = 0;
        this.leftoverTicks = 0;
        this.fps = 60;

        if (extRamBuffer) {
            this.loadExtRam(extRamBuffer);
        }

        this.bindKeys();
        this.gamepad.init();
    }

    destroy() {
        this.unbindKeys();
        this.gamepad.shutdown();
        this.cancelAnimationFrame();
        clearInterval(this.rewindIntervalId);
        this.rewind.destroy();
        this.module._emulator_delete(this.e);
        this.module._free(this.romDataPtr);
    }

    withNewFileData(cb) {
        const fileDataPtr = this.module._ext_ram_file_data_new(this.e);
        const buffer = makeWasmBuffer(
            this.module, this.module._get_file_data_ptr(fileDataPtr),
            this.module._get_file_data_size(fileDataPtr));
        const result = cb(fileDataPtr, buffer);
        this.module._file_data_delete(fileDataPtr);
        return result;
    }

    loadExtRam(extRamBuffer) {
        this.withNewFileData((fileDataPtr, buffer) => {
            if (buffer.byteLength === extRamBuffer.byteLength) {
                buffer.set(new Uint8Array(extRamBuffer));
                this.module._emulator_read_ext_ram(this.e, fileDataPtr);
            }
        });
    }

    getExtRam() {
        return this.withNewFileData((fileDataPtr, buffer) => {
            this.module._emulator_write_ext_ram(this.e, fileDataPtr);
            return new Uint8Array(buffer);
        });
    }

    get isPaused() {
        return this.rafCancelToken === null;
    }

    pause() {
        if (!this.isPaused) {
            this.cancelAnimationFrame();
            this.audio.pause();
            this.beginRewind();
        }
    }

    resume() {
        if (this.isPaused) {
            this.endRewind();
            this.requestAnimationFrame();
            this.audio.resume();
        }
    }

    setBuiltinPalette(pal) {
        this.module._emulator_set_builtin_palette(this.e, pal);
    }

    get isRewinding() {
        return this.rewind.isRewinding;
    }

    beginRewind() {
        this.rewind.beginRewind();
    }

    rewindToTicks(ticks) {
        if (this.rewind.rewindToTicks(ticks)) {
            this.runUntil(ticks);
            this.video.renderTexture();
        }
    }

    endRewind() {
        this.rewind.endRewind();
        this.lastRafSec = 0;
        this.leftoverTicks = 0;
        this.audio.startSec = 0;
    }

    set autoRewind(enabled) {
        if (enabled) {
            this.rewindIntervalId = setInterval(() => {
                const oldest = this.rewind.oldestTicks;
                const start = this.ticks;
                const delta =
                    REWIND_FACTOR * REWIND_UPDATE_MS / 1000 * CPU_TICKS_PER_SECOND;
                const rewindTo = Math.max(oldest, start - delta);
                this.rewindToTicks(rewindTo);
                BinjgbStore.ticks.set(BinjgbEmulator.instance.ticks);
            }, REWIND_UPDATE_MS);
        } else {
            clearInterval(this.rewindIntervalId);
            this.rewindIntervalId = 0;
        }
    }

    requestAnimationFrame() {
        this.rafCancelToken = requestAnimationFrame(this.rafCallback.bind(this));
    }

    cancelAnimationFrame() {
        cancelAnimationFrame(this.rafCancelToken);
        this.rafCancelToken = null;
    }

    run() {
        this.requestAnimationFrame();
    }

    get ticks() {
        return this.module._emulator_get_ticks_f64(this.e);
    }

    runUntil(ticks) {
        while (true) {
            const event = this.module._emulator_run_until_f64(this.e, ticks);
            if (event & EVENT_NEW_FRAME) {
                this.rewind.pushBuffer();
                this.video.uploadTexture();
            }
            if ((event & EVENT_AUDIO_BUFFER_FULL) && !this.isRewinding) {
                this.audio.pushBuffer();
            }
            if (event & EVENT_UNTIL_TICKS) {
                break;
            }
        }
        if (this.module._emulator_was_ext_ram_updated(this.e)) {
            BinjgbStore.extRamUpdated.set(true);
        }
    }

    rafCallback(startMs) {
        this.requestAnimationFrame();
        let deltaSec = 0;
        if (!this.isRewinding) {
            const startSec = startMs / 1000;
            deltaSec = Math.max(startSec - (this.lastRafSec || startSec), 0);
            const startTicks = this.ticks;
            const deltaTicks =
                Math.min(deltaSec, MAX_UPDATE_SEC) * CPU_TICKS_PER_SECOND;
            const runUntilTicks = (startTicks + deltaTicks - this.leftoverTicks);
            this.runUntil(runUntilTicks);
            this.leftoverTicks = (this.ticks - runUntilTicks) | 0;
            this.lastRafSec = startSec;
        }
        const lerp = (from, to, alpha) => (alpha * from) + (1 - alpha) * to;
        this.fps = lerp(this.fps, Math.min(1 / deltaSec, 10000), 0.3);
        this.video.renderTexture();
    }

    keyFuncs;
    boundKeyDown;
    boundKeyUp;
    bindKeys() {
        this.keyFuncs = {
            'ArrowDown': this.module._set_joyp_down.bind(null, this.e),
            'ArrowLeft': this.module._set_joyp_left.bind(null, this.e),
            'ArrowRight': this.module._set_joyp_right.bind(null, this.e),
            'ArrowUp': this.module._set_joyp_up.bind(null, this.e),
            'KeyZ': this.module._set_joyp_B.bind(null, this.e),
            'KeyX': this.module._set_joyp_A.bind(null, this.e),
            'Enter': this.module._set_joyp_start.bind(null, this.e),
            'Tab': this.module._set_joyp_select.bind(null, this.e),
            'Backspace': this.keyRewind.bind(this),
            'Space': this.keyPause.bind(this),
        };
        this.boundKeyDown = this.keyDown.bind(this);
        this.boundKeyUp = this.keyUp.bind(this);

        window.addEventListener('keydown', this.boundKeyDown);
        window.addEventListener('keyup', this.boundKeyUp);
    }

    unbindKeys() {
        window.removeEventListener('keydown', this.boundKeyDown);
        window.removeEventListener('keyup', this.boundKeyUp);
    }

    keyDown(event) {
        if (event.code in this.keyFuncs) {
            this.keyFuncs[event.code](true);
            event.preventDefault();
        }
    }

    keyUp(event) {
        if (event.code in this.keyFuncs) {
            this.keyFuncs[event.code](false);
            event.preventDefault();
        }
    }

    keyRewind(isKeyDown) {
        if (this.isRewinding !== isKeyDown) {
            if (isKeyDown) {
                BinjgbStore.paused.set(true);
                this.autoRewind = true;
            } else {
                BinjgbStore.paused.set(false);
                this.autoRewind = false;
            }
        }
    }

    keyPause(isKeyDown) {
        if (isKeyDown) BinjgbStore.togglePause();
    }
}