import { get } from "svelte/store";
import BinjgbStore from "./Binjgb.store";
import { makeWasmBuffer } from "./makeWasmBuffer";

export const AUDIO_FRAMES = 4096;
const AUDIO_LATENCY_SEC = 0.1;

export class BinjgbAudio {

  module;
  buffer;
  startSec;

  static ctx = new AudioContext();

  constructor(module, e) {
    this.module = module;
    this.buffer = makeWasmBuffer(
      this.module, this.module._get_audio_buffer_ptr(e),
      this.module._get_audio_buffer_capacity(e));
    this.startSec = 0;
    this.resume();
  }

  get sampleRate() { return BinjgbAudio.ctx.sampleRate; }

  pushBuffer() {
    const nowSec = BinjgbAudio.ctx.currentTime;
    const nowPlusLatency = nowSec + AUDIO_LATENCY_SEC;
    //   const volume = vm.volume;
    const volume = get(BinjgbStore.volume);
    this.startSec = (this.startSec || nowPlusLatency);
    if (this.startSec >= nowSec) {
      const buffer = BinjgbAudio.ctx.createBuffer(2, AUDIO_FRAMES, this.sampleRate);
      const channel0 = buffer.getChannelData(0);
      const channel1 = buffer.getChannelData(1);
      for (let i = 0; i < AUDIO_FRAMES; i++) {
        channel0[i] = this.buffer[2 * i] * volume / 255;
        channel1[i] = this.buffer[2 * i + 1] * volume / 255;
      }
      const bufferSource = BinjgbAudio.ctx.createBufferSource();
      bufferSource.buffer = buffer;
      bufferSource.connect(BinjgbAudio.ctx.destination);
      bufferSource.start(this.startSec);
      const bufferSec = AUDIO_FRAMES / this.sampleRate;
      this.startSec += bufferSec;
    } else {
      console.log(
        'Resetting audio (' + this.startSec.toFixed(2) + ' < ' +
        nowSec.toFixed(2) + ')');
      this.startSec = nowPlusLatency;
    }
  }

  pause() {
    BinjgbAudio.ctx.suspend();
  }

  resume() {
    BinjgbAudio.ctx.resume();
  }
}
