/*
 * Copyright (C) 2017 Ben Smith
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */


// const RESULT_ERROR = 1;







// const REWIND_FACTOR = 1.5;
// const REWIND_UPDATE_MS = 16;
// const BUILTIN_PALETTES = 83;  // See builtin-palettes.def.


// const $ = document.querySelector.bind(document);
// let emulator = null;

// const binjgbPromise = Binjgb();

// const dbPromise = idb.open('db', 1, upgradeDb => {
//   const objectStore = upgradeDb.createObjectStore('games', {keyPath : 'sha1'});
//   objectStore.createIndex('sha1', 'sha1', {unique : true});
// });

// let data = {
//   fps: 60,
//   ticks: 0,
//   loaded: false,
//   loadedFile: null,
//   paused: false,
//   extRamUpdated: false,
//   canvas: {
//     show: false,
//     useSgbBorder: false,
//     scale: 3,
//   },
//   rewind: {
//     minTicks: 0,
//     maxTicks: 0,
//   },
//   files: {
//     show: true,
//     selected: 0,
//     list: []
//   },
//   volume: 0.5,
//   pal: 0
// };

// let vm = new Vue({
//   el: '.main',
//   data: data,
//   created: function() {
//     setInterval(() => {
//       this.fps = emulator ? emulator.fps : 60;
//     }, 500);
//     setInterval(() => {
//       if (this.extRamUpdated) {
//         this.updateExtRam();
//         this.extRamUpdated = false;
//       }
//     }, 1000);
//     this.readFiles();
//   },
//   mounted: function() {
//     $('.main').classList.add('ready');
//   },
//   computed: {
//     canvasWidth: function() {
//       return this.canvas.useSgbBorder ? 256 : 160;
//     },
//     canvasHeight: function() {
//       return this.canvas.useSgbBorder ? 224 : 144;
//     },
//     canvasWidthPx: function() {
//       return (this.canvasWidth * this.canvas.scale) + 'px';
//     },
//     canvasHeightPx: function() {
//       return (this.canvasHeight * this.canvas.scale) + 'px';
//     },
//     rewindTime: function() {
//       const zeroPadLeft = (num, width) => ('' + (num | 0)).padStart(width, '0');
//       const ticks = this.ticks;
//       const hr = (ticks / (60 * 60 * CPU_TICKS_PER_SECOND)) | 0;
//       const min = zeroPadLeft((ticks / (60 * CPU_TICKS_PER_SECOND)) % 60, 2);
//       const sec = zeroPadLeft((ticks / CPU_TICKS_PER_SECOND) % 60, 2);
//       const ms = zeroPadLeft((ticks / (CPU_TICKS_PER_SECOND / 1000)) % 1000, 3);
//       return `${hr}:${min}:${sec}.${ms}`;
//     },
//     pauseLabel: function() {
//       return this.paused ? 'resume' : 'pause';
//     },
//     isFilesListEmpty: function() {
//       return this.files.list.length == 0;
//     },
//     loadedFileName: function() {
//       return this.loadedFile ? this.loadedFile.name : '';
//     },
//     selectedFile: function() {
//       return this.files.list[this.files.selected];
//     },
//     selectedFileHasImage: function() {
//       const file = this.selectedFile;
//       return file && file.image;
//     },
//     selectedFileImageSrc: function() {
//       if (!this.selectedFileHasImage) return '';
//       return this.selectedFile.image;
//     },
//   },
//   watch: {
//     paused: function(newPaused, oldPaused) {
//       if (!emulator) return;
//       if (newPaused == oldPaused) return;
//       if (newPaused) {
//         emulator.pause();
//         this.updateTicks();
//         this.rewind.minTicks = emulator.rewind.oldestTicks;
//         this.rewind.maxTicks = emulator.rewind.newestTicks;
//       } else {
//         emulator.resume();
//       }
//     },
//   },
//   methods: {
//     toggleFullscreen: function() { $('canvas').requestFullscreen(); },
//     palDown: function() { this.setPal(this.pal - 1); },
//     palUp: function() { this.setPal(this.pal + 1); },
//     setPal: function(pal) {
//       if (pal < 0) { pal = BUILTIN_PALETTES - 1; }
//       if (pal >= BUILTIN_PALETTES) { pal = 0; }
//       this.pal = pal;
//       if (emulator) { emulator.setBuiltinPalette(this.pal); }
//     },
//     updateTicks: function() {
//       this.ticks = emulator.ticks;
//     },
//     togglePause: function() {
//       if (!this.loaded) return;
//       this.paused = !this.paused;
//     },
//     rewindTo: function(event) {
//       if (!emulator) return;
//       emulator.rewindToTicks(+event.target.value);
//       this.updateTicks();
//     },
//     selectFile: function(index) {
//       this.files.selected = index;
//     },
//     playFile: async function(file) {
//       const [romBuffer, extRamBuffer] = await Promise.all([
//         readFile(file.rom),
//         file.extRam ? readFile(file.extRam) : Promise.resolve(null)
//       ]);
//       this.paused = false;
//       this.loaded = true;
//       this.canvas.show = true;
//       this.files.show = false;
//       this.loadedFile = file;
//       Emulator.start(await binjgbPromise, romBuffer, extRamBuffer);
//       emulator.setBuiltinPalette(this.pal);
//     },
//     updateExtRam: async function() {
//       if (!emulator) return;
//       const extRamBlob = new Blob([emulator.getExtRam()]);
//       const imageDataURL = $('canvas').toDataURL();
//       const db = await dbPromise;
//       const tx = db.transaction('games', 'readwrite');
//       const cursor = await tx.objectStore('games').openCursor(
//           this.loadedFile.sha1);
//       if (!cursor) return;
//       Object.assign(this.loadedFile, cursor.value);
//       this.loadedFile.extRam = extRamBlob;
//       this.loadedFile.image = imageDataURL;
//       this.loadedFile.modified = new Date;
//       cursor.update(this.loadedFile);
//       return tx.complete;
//     },
//   }
// });
