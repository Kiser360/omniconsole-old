import { get, writable } from "svelte/store";

export default {
    fps: writable(60),
    paused: writable(false),
    extRamUpdated: writable(false),
    ticks: writable(0),
    volume: writable(.5),

    togglePause() {
        this.paused.set(
            !get(this.paused)
        );
    }
}